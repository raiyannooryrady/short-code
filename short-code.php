<?php
/*
Plugin Name: Short Code
Plugin URI: https://liilab.com
Description: Count Words from any WordPress Post
Version: 1.0
Author: Rady
Author URI: https://raiyannooryrady-bd.netlify.app
License: GPLv2 or later
Text Domain: short_code
Domain Path: /languages/
*/
/*function wordcount_activation_hook(){}
register_activation_hook(__FILE__,"wordcount_activation_hook");

function wordcount_deactivation_hook(){}
register_deactivation_hook(__FILE__,"wordcount_deactivation_hook");*/
/* LOAD ASSETS */
function short_code_load_textdomain() {
	load_plugin_textdomain( 'short_code', false, dirname( __FILE__ ) . "/languages" );
}

add_action( 'plugins_loaded', 'short_code_load_textdomain' );
function sc_assets()
{
   // wp_enqueue_style("bootstrap-icons-css", "//cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css", null, "1.0");
   //wp_enqueue_style("bootstrap-css", "//cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css", null, "1.0");

//wp_enqueue_script("bootstrap-js", "https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js", null, "1.0");
}
add_action('wp_enqueue_scripts', 'sc_assets');

function short_code_button($attributes)
{

    $default = array(
        'type' => 'primary',
        'title' => __("Button", 'short_code'),
        'text-color' => __("dark", 'short_code'),
        'url' => '',
    );

    $button_attributes = shortcode_atts($default, $attributes);


    return sprintf(
        '<a target="_blank" class="btn btn-%s text-%s full-width" href="%s">%s</a>',
        $button_attributes['type'],
        $button_attributes['text-color'],
        $button_attributes['url'],
        $button_attributes['title'],

    );
}

add_shortcode('button', 'short_code_button');


function short_code_button2($attributes, $content = '')
{
    $default = array(
        'type' => 'primary',
        'title' => __("Button", 'short_code'),
        'text-color' => __("dark", 'short_code'),
        'url' => '',
    );

    $button_attributes = shortcode_atts($default, $attributes);


    return sprintf(
        '<a target="_blank" class="btn btn-%s text-%s full-width" href="%s">%s</a>',
        $button_attributes['type'],
        $button_attributes['text-color'],
        $button_attributes['url'],
        do_shortcode($content)
    );
}

add_shortcode('button2', 'short_code_button2');

function short_code_uppercase($attributes, $content = '')
{
    return strtoupper(do_shortcode($content));
}
add_shortcode('uc', 'short_code_uppercase');

function short_code_google_map($attributes)
{
    $default = array(
        'place' => 'Dhaka Museum',
        'width' => '800',
        'height' => '500',
        'zoom' => '14'
    );

    $params = shortcode_atts($default, $attributes);

    $map = <<<EOD
<div>
    <div>
        <iframe width="{$params['width']}" height="{$params['height']}"
                src="https://maps.google.com/maps?q={$params['place']}&t=&z={$params['zoom']}&ie=UTF8&iwloc=&output=embed"
                frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
        </iframe>
    </div>
</div>
EOD;

    return $map;
}
add_shortcode('gmap', 'short_code_google_map');
